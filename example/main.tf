locals {
  assetname = "rick"
  environment = "example"
  location = "westus"

  resource_name = format("%s-%s-%s", local.assetname, local.environment, local.location)
}

resource "azurerm_resource_group" "resourcegroup" {
  name = "${local.resource_name}-rg-1"
  location = local.location
}


module "virtual_machine" {
  source = "git::https://gitlab.com/Richardbmk/virtual-machine.git?ref=v1.0.0"

  instance_count          = 1
  vmname                  = "mynewvm-${local.environment}"
  resource_group_location = azurerm_resource_group.resourcegroup.location
  resource_group_name     = azurerm_resource_group.resourcegroup.name
  environment             = local.environment
  network_interface_ids   = module.network-interface.nic_id
  vm_size                 = var.vm_size
  os_disk_type            = var.os_disk_type
  admin_usename           = var.admin_usename
  admin_password          = var.admin_password
  image_publisher         = var.image_publisher
  image_offer             = var.image_offer
  image_sku               = var.image_sku
}